# vmcore analysis: some hints at finding the input values of funtions
We want to find the inputs for `assoc_array_insert_into_terminal_node` in this partial stack. The system crashed in this function (*called function*), and was called by `assoc_array_insert` (*callee function*).
~~~
    [exception RIP: assoc_array_insert_into_terminal_node+1690]
    RIP: ffffffff8fca97fa  RSP: ffffb1d8c89b39b0  RFLAGS: 00010286
    RAX: ffffa05c830ada98  RBX: ffffa05c85f26000  RCX: 0000000000000000
    RDX: ffffa05c85f26001  RSI: 000000000000000b  RDI: 0000000000000001
    RBP: ffffffff906553c0   R8: ffffa06865f5862b   R9: ffffffffc082c080
    R10: 0000000000313070  R11: 000000000000000b  R12: ffffa05c830ada40
    R13: ffffa0635051c180  R14: 0000000000000090  R15: 00000000ffffffff
    ORIG_RAX: ffffffffffffffff  CS: 0010  SS: 0018
 #7 [ffffb1d8c89b3a38] assoc_array_insert at ffffffff8fca9c83
 #8 [ffffb1d8c89b3ad0] __key_link_begin at ffffffff8fbea65c


crash> whatis assoc_array_insert_into_terminal_node
bool assoc_array_insert_into_terminal_node(struct assoc_array_edit *, // RDI
                     const struct assoc_array_ops *,                  // RSI
                     const void *,                                    // RDX
                      struct assoc_array_walk_result *);              // RCX

~~~
[Check the calling conventions for the architecture at os-dev wiki](https://wiki.osdev.org/Calling_Conventions).

`rdx` and `rsi` are covered by fregs.
~~~
crash> fregs
...
assoc_array_insert_into_terminal_node called from 0xffffffff8fca9c83 <assoc_array_insert+483>
 +R12: 0x0
 +R13: 0xffffffff906553c0
 +R14: 0xffffb1d8c89b3b28
 +R15: 0xffffa055e04607b0
3 RDX: 0xffffb1d8c89b3b28
2 RSI: 0xffffffff906553c0
...
~~~
### How to find `rdi`?
First, we need to check the callee function to find where it takes the values from.
~~~
crash> dis -lr ffffffff8fca9c83 | tail
/usr/src/debug/kernel-4.18.0-348.el8/linux-4.18.0-348.el8.x86_64/lib/assoc_array.c: 1003
0xffffffff8fca9c70 <assoc_array_insert+464>:    lea    0x20(%rsp),%rcx
0xffffffff8fca9c75 <assoc_array_insert+469>:    mov    %r14,%rdx
0xffffffff8fca9c78 <assoc_array_insert+472>:    mov    %r13,%rsi
0xffffffff8fca9c7b <assoc_array_insert+475>:    mov    %rbx,%rdi     <---
0xffffffff8fca9c7e <assoc_array_insert+478>:    callq  0xffffffff8fca9160 <assoc_array_insert_into_terminal_node>
~~~
`rdi` is taken from `rbx`, which is a preserved register.

A preserved register is saved to the stack by the next function that will use it. This is done at the start of the function, by a `push` operation. Let's see `assoc_array_insert_into_terminal_node` pushed this value to the stack.
~~~
crash> dis assoc_array_insert_into_terminal_node | grep push
0xffffffff8fca9160 <assoc_array_insert_into_terminal_node>:	push   %r15
0xffffffff8fca9168 <assoc_array_insert_into_terminal_node+8>:	push   %r14
0xffffffff8fca916d <assoc_array_insert_into_terminal_node+13>:	push   %r13
0xffffffff8fca916f <assoc_array_insert_into_terminal_node+15>:	push   %r12
0xffffffff8fca9174 <assoc_array_insert_into_terminal_node+20>:	push   %rbp
0xffffffff8fca9175 <assoc_array_insert_into_terminal_node+21>:	push   %rbx <---
~~~
Let's find it in the stack.
~~~
crash> bt -f
...
    [exception RIP: assoc_array_insert_into_terminal_node+1690]
    RIP: ffffffff8fca97fa  RSP: ffffb1d8c89b39b0  RFLAGS: 00010286
    RAX: ffffa05c830ada98  RBX: ffffa05c85f26000  RCX: 0000000000000000
    RDX: ffffa05c85f26001  RSI: 000000000000000b  RDI: 0000000000000001
    RBP: ffffffff906553c0   R8: ffffa06865f5862b   R9: ffffffffc082c080
    R10: 0000000000313070  R11: 000000000000000b  R12: ffffa05c830ada40
    R13: ffffa0635051c180  R14: 0000000000000090  R15: 00000000ffffffff
    ORIG_RAX: ffffffffffffffff  CS: 0010  SS: 0018
    ffffb1d8c89b3908: 00000000ffffffff 0000000000000090
    ffffb1d8c89b3918: ffffa0635051c180 ffffa05c830ada40
    ffffb1d8c89b3928: ffffffff906553c0 ffffa05c85f26000
    ffffb1d8c89b3938: 000000000000000b 0000000000313070
    ffffb1d8c89b3948: ffffffffc082c080 ffffa06865f5862b
    ffffb1d8c89b3958: ffffa05c830ada98 0000000000000000
    ffffb1d8c89b3968: ffffa05c85f26001 000000000000000b
    ffffb1d8c89b3978: 0000000000000001 ffffffffffffffff
    ffffb1d8c89b3988: ffffffff8fca97fa 0000000000000010
    ffffb1d8c89b3998: 0000000000010286 ffffb1d8c89b39b0
    ffffb1d8c89b39a8: 0000000000000018 ffffa05c85f26001
    ffffb1d8c89b39b8: e5625c1500000000 ffffb1d8c89b3b28
    ffffb1d8c89b39c8: ffffa05c830add40 ffffa05c830ada41
    ffffb1d8c89b39d8: 00000000ffffffff ffffa05c830ada40
    ffffb1d8c89b39e8: ffffa05c830add41 e5625c1597a34300
    ffffb1d8c89b39f8: ffffa0635051c181 ffffffff8fca8e70
    ffffb1d8c89b3a08: ffffa05c85f26000 ffffb1d8c89b3ac8 <--- rbx, rbp
    ffffb1d8c89b3a18: 0000000000000000 ffffffff906553c0 <--- r12, r13
    ffffb1d8c89b3a28: ffffb1d8c89b3b28 ffffa055e04607b0 <--- r14, r15
    ffffb1d8c89b3a38: ffffffff8fca9c83                  <--- return address
 #7 [ffffb1d8c89b3a38] assoc_array_insert at ffffffff8fca9c83
 ...
~~~

In the `bt -f` presentation, the stack grows upwards, right-to-left. I fond useful to annotate the registers as I did above to avoid confusion. `rdi` is `ffffa05c85f26000`.

### How to find `rcx`?
~~~
crash> dis -lr ffffffff8fca9c83 | tail
/usr/src/debug/kernel-4.18.0-348.el8/linux-4.18.0-348.el8.x86_64/lib/assoc_array.c: 1003
0xffffffff8fca9c70 <assoc_array_insert+464>:    lea    0x20(%rsp),%rcx   <---
0xffffffff8fca9c75 <assoc_array_insert+469>:    mov    %r14,%rdx
0xffffffff8fca9c78 <assoc_array_insert+472>:    mov    %r13,%rsi
0xffffffff8fca9c7b <assoc_array_insert+475>:    mov    %rbx,%rdi
0xffffffff8fca9c7e <assoc_array_insert+478>:    callq  0xffffffff8fca9160 <assoc_array_insert_into_terminal_node>
~~~
`lea` is used to copy the address without reading the memory. To find `rcx`, we need to find `rsp` and add `0x20` to it.

`rsp` is the register that points to the top of the stack. It is changed by `push` and `pop` operations, or by direct manipulation. The `rsp` value when the function was called is shown in `bt` as the number in front of the callee function.
~~~
 #8 [ffffb1d8c89b3ad0] __key_link_begin at ffffffff8fbea65c
     ^^^^^^^^^^^^^^^^
 ~~~
 Let's see what the the callee does to `rsp`:
 ~~~
 crash> dis -r ffffffff8fca9c83 | head
0xffffffff8fca9aa0 <assoc_array_insert>:	push   %rbp
0xffffffff8fca9aa1 <assoc_array_insert+1>:	mov    %rsp,%rbp
0xffffffff8fca9aa4 <assoc_array_insert+4>:	push   %r15
0xffffffff8fca9aa6 <assoc_array_insert+6>:	push   %r14
0xffffffff8fca9aa8 <assoc_array_insert+8>:	push   %r13
0xffffffff8fca9aaa <assoc_array_insert+10>:	push   %r12
0xffffffff8fca9aac <assoc_array_insert+12>:	push   %rbx
0xffffffff8fca9aad <assoc_array_insert+13>:	and    $0xfffffffffffffff0,%rsp
0xffffffff8fca9ab1 <assoc_array_insert+17>:	sub    $0x60,%rsp
~~~
`rsp` is copied to `rbp` after one `push`. We don't have a record of `rbp`, so we can't use it.

The `rsp` at the entry point of the function is `ffffb1d8c89b3ad0`, so let's calculate it. In the beginning, the function calls `push` 6 times. We subtract 8 bytes for each push (the stack on `x86` grows downwards).
~~~
crash> dis -r ffffffff8fca9c83 | grep push
0xffffffff8fca9aa0 <assoc_array_insert>:	push   %rbp
0xffffffff8fca9aa4 <assoc_array_insert+4>:	push   %r15
0xffffffff8fca9aa6 <assoc_array_insert+6>:	push   %r14
0xffffffff8fca9aa8 <assoc_array_insert+8>:	push   %r13
0xffffffff8fca9aaa <assoc_array_insert+10>:	push   %r12
0xffffffff8fca9aac <assoc_array_insert+12>:	push   %rbx

crash> p/x 0xffffb1d8c89b3ad0 - 6 * 8
$8 = 0xffffb1d8c89b3aa0
~~~
Then we see what else is done to `rsp` and repeat those steps.
~~~
0xffffffff8fca9aad <assoc_array_insert+13>:	and    $0xfffffffffffffff0,%rsp
0xffffffff8fca9ab1 <assoc_array_insert+17>:	sub    $0x60,%rsp

crash> p/x 0xffffb1d8c89b3aa0 & 0xfffffffffffffff0
$9 = 0xffffb1d8c89b3aa0

crash> p/x 0xffffb1d8c89b3aa0 - 0x60
$10 = 0xffffb1d8c89b3a40

crash> p/x 0xffffb1d8c89b3a40 + 0x20
$11 = 0xffffb1d8c89b3a60
~~~
`rsp` is `0xffffb1d8c89b3a40`, so `rcx = rsp + 0x20 = 0xffffb1d8c89b3a60`.
~~~
crash> assoc_array_walk_result -x 0xffffb1d8c89b3a60
struct assoc_array_walk_result {
  terminal_node = {
    node = 0xffffa0635051c180, 
    level = 0x0, 
    slot = 0x5
  }, 
  wrong_shortcut = {
    shortcut = 0xffffffffc082c080 <key_type_dns_resolver>, 
    level = 0x0, 
    sc_level = 0x0, 
    sc_segments = 0xffffffff9017d59c, 
    dissimilarity = 0xffffffff8fbe8c09
  }
}
~~~
